package com.bitct.conf;

import com.bitct.vo.ServiceGroup;
import com.google.common.collect.Lists;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * @author JesseYang
 */
@Configuration
@ConfigurationProperties("service-list")
public class SystemConfig {

    private final List<ServiceGroup> groupList = Lists.newArrayList();

    public List<ServiceGroup> getGroupList() {

        return groupList;
    }


}
