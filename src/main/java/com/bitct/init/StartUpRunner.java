package com.bitct.init;

import com.bitct.service.CommandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * @author JesseYang
 */
@Component
public class StartUpRunner implements CommandLineRunner {

    @Autowired
    private CommandService commandService;

    @Override
    public void run(String... arg0) throws Exception {
        commandService.checkAllByMultiThreads();
    }

}
